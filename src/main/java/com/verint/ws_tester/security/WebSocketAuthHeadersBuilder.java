package com.verint.ws_tester.security;

import org.springframework.http.HttpHeaders;
import org.springframework.web.socket.WebSocketHttpHeaders;

import java.util.Objects;

public class WebSocketAuthHeadersBuilder {

    public static WebSocketHttpHeaders build(Credentials credentials) {
        HttpHeaders httpHeaders = new HttpHeaders();
        if(credentials.getUsername() != null) {
            httpHeaders.add("Authorization", "Basic " + credentials.getBase64Authorization());
        }
        return new WebSocketHttpHeaders(httpHeaders);
    }
}
