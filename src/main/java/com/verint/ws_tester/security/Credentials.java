package com.verint.ws_tester.security;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Base64;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Credentials {
    private String username;
    private String password;

    public String getPlainAuthorization() {
        return username + ":" + password;
    }

    public String getBase64Authorization() {
        return new String(Base64.getEncoder().encode(getPlainAuthorization().getBytes()));
    }
}