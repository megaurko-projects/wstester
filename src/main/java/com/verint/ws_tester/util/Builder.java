package com.verint.ws_tester.util;

public interface Builder<T> {
    T build();
}
