package com.verint.ws_tester.service;

import lombok.extern.log4j.Log4j2;
import org.springframework.messaging.simp.stomp.StompFrameHandler;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.messaging.simp.stomp.StompSessionHandlerAdapter;
import org.springframework.web.socket.WebSocketHttpHeaders;
import org.springframework.web.socket.messaging.WebSocketStompClient;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ExecutionException;

@Log4j2
public class StompSessionConnectionRoutingHandlerAdapter extends StompSessionHandlerAdapter {

    private final WebSocketStompClient stompClient;

    private StompSession session;
    private Map<String, StompFrameHandler> destinationHandlers = new HashMap<>();

    public static StompSessionConnectionRoutingHandlerAdapter from(WebSocketStompClient stompClient) {
        return new StompSessionConnectionRoutingHandlerAdapter(stompClient);
    }

    private StompSessionConnectionRoutingHandlerAdapter(WebSocketStompClient stompClient) {
        this.stompClient = stompClient;
    }

    @Override
    public void afterConnected(StompSession session, StompHeaders connectedHeaders) {
        destinationHandlers.forEach(session::subscribe);
    }

    public void destroy() throws Exception {
        disconnect();
    }

    public void disconnect() {
        if (session != null && session.isConnected()) {
            session.disconnect();
            log.debug("Stomp session disconnected.");
        }
    }

    public void connect(String host) throws ExecutionException, InterruptedException {
        connect(host, null);
    }

    public void connect(String host, WebSocketHttpHeaders headers) throws ExecutionException, InterruptedException {
        if (session == null || !session.isConnected()) {
            session = stompClient.connect(host, headers, this).get();
            log.debug("Stomp session connected.");
        }
    }

    public void restartStompClient() {
        stompClient.stop();
        stompClient.start();
        log.debug("Stomp client restarted.");
    }

    public StompSessionConnectionRoutingHandlerAdapter addRouteHandler(String dst, StompFrameHandler frameHandler) {
        destinationHandlers.put(dst, frameHandler);
        return this;
    }

    public void send(String dst, Object requestMsg) {
        Objects.requireNonNull(dst);
        if (this.session != null && this.session.isConnected()) {
            this.session.send(dst, requestMsg);
        }
    }
}
