package com.verint.ws_tester;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.verint.ws_tester.cli.AppInitializer;
import com.verint.ws_tester.cli.AppInitializerImpl;
import com.verint.ws_tester.security.Credentials;
import com.verint.ws_tester.security.WebSocketAuthHeadersBuilder;
import com.verint.ws_tester.service.StompSessionConnectionRoutingHandlerAdapter;
import lombok.Builder;
import lombok.extern.log4j.Log4j2;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.messaging.simp.stomp.StompSessionHandlerAdapter;
import org.springframework.web.socket.WebSocketHttpHeaders;
import org.springframework.web.socket.messaging.WebSocketStompClient;
import picocli.CommandLine;

import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Builder
@Log4j2
public class WsTesterApplication implements AppRunner {

    private AppInitializer config;
    private WebSocketStompClient client;
    private StompSessionConnectionRoutingHandlerAdapter sessionAdapter;
    private ObjectMapper objectMapper;

    public static void main(String[] args) {
        CommandLine.call(new AppInitializerImpl(), args);
    }

    @Override
    public void run() {

        try {
            ExecutorService threadPool = Executors.newFixedThreadPool(1);
            threadPool.submit(() -> {
                try {
                    sessionAdapter.connect(config.getHost(), createAuthHeadersBuilder());

                    handleRequest();
                } catch (ExecutionException | InterruptedException | IOException e) {
                    log.error(e);
                }
            });

            Runtime.getRuntime()
                    .addShutdownHook(new Thread(threadPool::shutdownNow));
        } catch (Exception e) {
            log.error(e);
        }
    }

    private void handleRequest() throws IOException {
        Map<String, Object> dynamicObject = prepareRequestBody();

        if (config.getRequestDst() != null) {
            sessionAdapter.send(
                    config.getRequestDst(),
                    objectMapper.writeValueAsBytes(dynamicObject));
        }
    }

    private Map<String, Object> prepareRequestBody() throws IOException {
        String requestBodyFile = config.getRequestBodyFile();
        TypeReference<HashMap<String, Object>> valueTypeRef = new TypeReference<HashMap<String, Object>>() {};
        if(requestBodyFile != null) {
            return objectMapper.readValue(Paths.get(requestBodyFile).toFile(), valueTypeRef);
        } else if (config.getRequestBody() != null) {
            return objectMapper.readValue(config.getRequestBody(), valueTypeRef);
        }
        return Collections.emptyMap();
    }

    private WebSocketHttpHeaders createAuthHeadersBuilder() {
        return WebSocketAuthHeadersBuilder.build(new Credentials(config.getUsername(), config.getPassword()));
    }

    public static class StompHandler extends StompSessionHandlerAdapter {

        @Override public Type getPayloadType(StompHeaders headers) {
            return Object.class;
        }

        @Override
        public void handleFrame(StompHeaders headers, Object payload) {
            log.info(payload.toString());
        }

        @Override public void handleException(
                StompSession session,
                StompCommand command,
                StompHeaders headers,
                byte[] payload,
                Throwable exception
        ) {
            log.error(exception.getMessage());
        }

        @Override
        public void handleTransportError(StompSession session, Throwable exception) {
            log.error(exception.getMessage());
        }
    }

}
