package com.verint.ws_tester.cli;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.verint.ws_tester.AppRunner;
import com.verint.ws_tester.WebSocketStompClientBuilder;
import com.verint.ws_tester.WsTesterApplication;
import com.verint.ws_tester.security.SecurityConfig;
import com.verint.ws_tester.service.StompSessionConnectionRoutingHandlerAdapter;
import org.springframework.web.socket.messaging.WebSocketStompClient;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;

/**
 * The implementation builds tester application using Web Socket client and Jackson json object mapper.
 */
public class AppInitializerImpl extends AppInitializer {

    @Override
    public AppRunner buildApp() throws KeyManagementException, NoSuchAlgorithmException {
        return WsTesterApplication.builder()
                .sessionAdapter(createAdapter(createWsClient()))
                .objectMapper(new ObjectMapper())
                .config(this)
                .build();
    }

    /**
     * Create ws client using SSL context
     */
    private WebSocketStompClient createWsClient() throws NoSuchAlgorithmException, KeyManagementException {
        WebSocketStompClientBuilder clientBuilder = WebSocketStompClientBuilder.create();
        if (getHost().startsWith("wss")) {
            clientBuilder.serverType(WebSocketStompClientBuilder.ServerType.UNDERTOW)
                    .sslContext(this.isDefaultTrustManager()
                                        ? SecurityConfig.defaultSSLContext()
                                        : SecurityConfig.allTrustingSSLContext());
        }
        return clientBuilder.build();
    }

    /**
     * Create adapter from client and add response listeners.
     */
    private StompSessionConnectionRoutingHandlerAdapter createAdapter(WebSocketStompClient client) {
        StompSessionConnectionRoutingHandlerAdapter sessionAdapter =
                StompSessionConnectionRoutingHandlerAdapter.from(client);
        getResponseDst().forEach(dst -> sessionAdapter.addRouteHandler(dst, new WsTesterApplication.StompHandler()));
        return sessionAdapter;
    }
}
