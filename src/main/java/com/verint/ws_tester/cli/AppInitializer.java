package com.verint.ws_tester.cli;

import com.verint.ws_tester.AppRunner;
import lombok.Getter;
import picocli.CommandLine;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

/**
 * Initializes application components from CLI parameters and runs the application.
 * If CLI parameter validation fails, prints help to console.
 */
@Getter
@CommandLine.Command(
        description = "STOMP WebSocket testing utility.",
        name = "ws-tester",
        mixinStandardHelpOptions = true,
        version = "1.0")
public abstract class AppInitializer implements Callable<Void> {

    @CommandLine.Parameters(index = "0", description = "Web socket host to connect to.")
    private String host;

    @CommandLine.Option(names = {"-u", "--username"}, description = "Host username.")
    private String username;

    @CommandLine.Option(names = {"-p", "--password"}, description = "Host password.")
    private String password;

    @CommandLine.Option(names = {"-qD", "--requestDst"}, description = "Channel to send request to.")
    private String requestDst;

    @CommandLine.Option(names = {"-qX", "--requestBody"}, description = "Request body object.")
    private String requestBody;

    @CommandLine.Option(names = {"-qXf", "--requestBodyFile"}, description = "Request body file path.")
    private String requestBodyFile;

    @CommandLine.Option(names = {"-sD", "--responseDst"}, description = "Subscription channel to listen to.")
    private List<String> responseDst = new ArrayList<>();

    @CommandLine.Option(names = {"-o", "--outputPath"}, description = "Output file path.")
    private String outputPath;

    @CommandLine.Option(
            names = {"--dtm"},
            description = "Use default Java trust manager. Default: all trusting manager.")
    private boolean isDefaultTrustManager;

    @Override
    public Void call() throws Exception {
        buildApp().run();
        return null;
    }

    public abstract AppRunner buildApp() throws Exception;
}
