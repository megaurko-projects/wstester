package com.verint.ws_tester;

import com.verint.ws_tester.util.Builder;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.converter.MessageConverter;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.messaging.WebSocketStompClient;

import javax.net.ssl.SSLContext;
import java.util.HashMap;
import java.util.Objects;

public class WebSocketStompClientBuilder implements Builder<WebSocketStompClient> {

    private SSLContext sslContext;
    private ServerType serverType = ServerType.UNDERTOW;
    private MessageConverter messageConverter = new MappingJackson2MessageConverter();

    @AllArgsConstructor
    @Getter
    public enum ServerType {
        TOMCAT("org.apache.tomcat.websocket.SSL_CONTEXT"),
        UNDERTOW("io.undertow.websocket.SSL_CONTEXT");

        private String sslContextConstant;
    }

    public static WebSocketStompClientBuilder create() {
        return new WebSocketStompClientBuilder();
    }

    private WebSocketStompClientBuilder() {}

    public WebSocketStompClientBuilder sslContext(SSLContext sslContext) {
        this.sslContext = sslContext;
        return this;
    }

    public WebSocketStompClientBuilder serverType(ServerType serverType) {
        this.serverType = serverType;
        return this;
    }

    public WebSocketStompClientBuilder messageConverter(MessageConverter messageConverter) {
        this.messageConverter = messageConverter;
        return this;
    }

    @Override
    public WebSocketStompClient build() {

        Objects.requireNonNull(messageConverter);

        StandardWebSocketClient client = new StandardWebSocketClient();

        if (sslContext != null && serverType != null) {
            client.setUserProperties(new HashMap<String, Object>() {{
                put(serverType.getSslContextConstant(), sslContext);
            }});
        }

        WebSocketStompClient stompClient = new WebSocketStompClient(client);
        stompClient.setMessageConverter(messageConverter);
        return stompClient;
    }
}
