java -jar ws-tester.jar ^
    -qD /app/queue/message ^
    -qX "{ \"msg\": \"Hello!\" }" ^
    -sD /dst/broadcast ^
    -sD /user/response/private ^
    -u admin ^
    -p password ^
    wss://localhost:8443/ugs